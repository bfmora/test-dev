import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import { FormsModule }    from '@angular/forms';

import { AppComponent } from './app.component';
import { RequestService } from './services/requests.service';
import { MessageService } from './services/message.service';
import { InvoicesComponent } from './invoices/invoices.component';
import { InvoiceDetailComponent } from './invoice-details/invoice-details.component';

@NgModule({
  declarations: [
    AppComponent,
    InvoicesComponent,
    InvoiceDetailComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    AppRoutingModule,
    HttpClientModule
  ],
  providers: [RequestService, MessageService],
  bootstrap: [AppComponent]
})
export class AppModule { }
