import { Component, OnInit } from '@angular/core';

import { Result } from '../data-models/result';
import { Invoice } from '../data-models/invoices';
import { RequestService } from '../services/requests.service';

@Component({
  selector: 'app-invoices',
  templateUrl: './invoices.component.html',
  styleUrls: ['./invoices.component.css']
})
export class InvoicesComponent implements OnInit {
  result: Result;
  invoices: Invoice[];

  constructor(private RequestService: RequestService) { }

  ngOnInit() {
    this.getInvoices();
  }

  getInvoices(): void {
    this.RequestService.getInvoice()
    .subscribe(result => this.invoices = JSON.parse(result.Data));
  }

}
