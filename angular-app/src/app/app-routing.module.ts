import { NgModule }             from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { InvoicesComponent }      from './invoices/invoices.component';
import { InvoiceDetailComponent }  from './invoice-details/invoice-details.component';

const routes: Routes = [
  { path: '', redirectTo: '', pathMatch: 'full' },
  { path: 'dashboard', component: InvoicesComponent },
  { path: 'detail/:id', component: InvoiceDetailComponent },
  { path: 'invoices', component: InvoicesComponent }
];

@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})
export class AppRoutingModule {}
