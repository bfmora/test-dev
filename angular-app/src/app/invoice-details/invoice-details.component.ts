import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';

import { Result } from '../data-models/result';
import { Invoice, InvoiceDetails } from '../data-models/invoices';
import { RequestService } from '../services/requests.service';

@Component({
  selector: 'invoice-details',
  templateUrl: './invoice-details.component.html',
  styleUrls: ['./invoice-details.component.css']
})
export class InvoiceDetailComponent implements OnInit {
  @Input() invoiceDetail: InvoiceDetails;
  result: Result;
  name: string;

  constructor(
    private route: ActivatedRoute,
    private requestService: RequestService,
    private location: Location
  ) {}

  ngOnInit() {
    this.getInvoices();
    +this.route.snapshot.paramMap.get('name');
  }

  getInvoices(): void {
    const id = +this.route.snapshot.paramMap.get('id');
    this.requestService.getInvoiceDetails(id)
      .subscribe(result => this.invoiceDetail = JSON.parse(result.Data));
  }

  goBack(): void {
    this.location.back();
  }
}
