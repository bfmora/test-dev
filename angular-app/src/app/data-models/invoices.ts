export class Invoice {
    InvoiceDetailsID: number;
    InvoiceID: number;
    InvoiceName: string;
}

export class InvoiceDetails {
    InvoiceDetailsID: number;
    InvoiceConcept: string;
    InvoiceTotalValue: number;
    InvoiceDate: string;
}