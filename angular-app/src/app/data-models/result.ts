export class Result {
    Code: number;
    Message: string;
    Data: string;
}