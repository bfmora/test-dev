import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';
import { catchError, map, tap } from 'rxjs/operators';

import { Result } from '../data-models/result';
import { MessageService } from './message.service';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable()
export class RequestService {

  private invoiceUrl = 'http://localhost:63316/api/invoices';  // URL to web api
  private invoiceDetails = 'http://localhost:63316/api/invoicedetail';  // URL to web api

  constructor(
    private http: HttpClient,
    private messageService: MessageService) { }

  /** GET invoices from the server */
  getInvoice (): Observable<Result> {
    return this.http.get<Result>(this.invoiceUrl).pipe(
      tap(_ => this.log(`fetched invoices`)),
      catchError(this.handleError<Result>(`getInvoices`))
    );
  }

  /** GET invoice details by id. Return `undefined` when id not found */
  getInvoiceNo404<Data>(id: number): Observable<Result> {
    const url = `${this.invoiceDetails}/${id}`;
    return this.http.get<Result[]>(url)
      .pipe(
        map(heroes => heroes[0]), // returns a {0|1} element array
        tap(h => {
          const outcome = h ? `fetched` : `did not find`;
          this.log(`${outcome} hero id=${id}`);
        }),
        catchError(this.handleError<Result>(`getHero id=${id}`))
      );
  }

  /** GET invoice details by id. Will 404 if id not found */
  getInvoiceDetails(id: number): Observable<Result> {
    const url = `${this.invoiceDetails}/${id}`;
    return this.http.get<Result>(url).pipe(
      tap(_ => this.log(`fetched hero id=${id}`)),
      catchError(this.handleError<Result>(`getHero id=${id}`))
    );
  }

  /**
   * Handle Http operation that failed.
   * Let the app continue.
   * @param operation - name of the operation that failed
   * @param result - optional value to return as the observable result
   */
  private handleError<T> (operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {

      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead

      // TODO: better job of transforming error for user consumption
      this.log(`${operation} failed: ${error.message}`);

      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }

  /** Log a HeroService message with the MessageService */
  private log(message: string) {
    this.messageService.add('HeroService: ' + message);
  }
}
