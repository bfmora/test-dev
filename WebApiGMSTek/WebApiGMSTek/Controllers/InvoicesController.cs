﻿//-----------------------------------------------------------------------
// <copyright file="InvoicesController.cs" company="Development">
//     Copyright . All rights reserved.
// </copyright>
// <author>Brayan Mora</author>
// <description></description>
//-----------------------------------------------------------------------

namespace WebApiGMSTek.Controllers
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Web.Http;
    using System.Web.Http.Cors;
    using System.Web.Script.Serialization;
    using WebApiGMSTek.Models;

    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class InvoicesController : ApiController
    {
        private readonly DBString db;

        // Re uses context in case is avaliable
        public InvoicesController()
        {
            db = db ?? new DBString();
        }

        // GET: api/Invoices
        [Route("api/Invoices")]
        public Result Get()
        {
            Result ret = new Result();
            List<Invoice> data = db.Invoice.ToList();

            if (data == null)
            {
                ret.Code = 0;
                ret.Message = "The is no data avaliable";
            }
            else {
                var jS = new JavaScriptSerializer();
                List<InvoiceModel> sData = new List<InvoiceModel>();
                data.ForEach(m => {
                    var model = new InvoiceModel
                    {
                        InvoiceDetailsID = m.InvoiceDetailsID,
                        InvoiceID = m.InvoiceID,
                        InvoiceName = m.InvoiceName
                    };
                    sData.Add(model);
                });
                ret.Code = 1;
                ret.Message = "Sucess!";           
                ret.Data = jS.Serialize(sData);
            }

            return ret;
        }

        // DELETE: api/Invoices/{id}
        [Route("api/Invoices/{id}")]
        [HttpDelete]
        public Result DeleteInvoice(int id)
        {
            Result ret = new Result();
            Invoice data = db.Invoice.Where(m => m.InvoiceID == id).FirstOrDefault();

            if (data == null)
            {
                ret.Code = 0;
                ret.Message = "The record does not exist";
            }
            else
            {
                try
                {
                    db.InvoiceDetails.Remove(data.InvoiceDetails);
                    db.Invoice.Remove(data);
                    db.SaveChanges();
                    ret.Code = 1;
                    ret.Message = "The record " + data.InvoiceID + " was deleted sucessfully!";
                }
                catch (System.Exception ex)
                {
                    ret.Code = 0;
                    ret.Message = ex.Message.ToString();
                }
            }
            return ret;
        }

        // POST: api/Invoices
        [Route("api/Invoices")]
        [HttpPost]
        public Result CreateInvoice([FromBody] Invoice param)
        {
            Result ret = new Result();
            Invoice data = db.Invoice.Where(m => m.InvoiceID == param.InvoiceID).FirstOrDefault();

            if (data != null)
            {
                ret.Code = 0;
                ret.Message = "The record already exist";
            }
            else
            {
                try
                {
                    data = new Invoice
                    {
                        InvoiceName = param.InvoiceName,
                        CreatedOn = System.DateTime.Now,
                        ModifiedOn = System.DateTime.Now,
                        Active = true
                    };

                    db.Invoice.Add(data);
                    db.SaveChanges();
                    ret.Code = 1;
                    ret.Message = "The record " + data.InvoiceID + " was created sucessfully!";
                }
                catch (System.Exception ex)
                {
                    ret.Code = 0;
                    ret.Message = ex.Message.ToString();
                }
            }
            return ret;
        }

        // POST: api/Invoices/{id}
        [Route("api/Invoices/{id}")]
        [HttpPut]
        public Result UpdateInvoice([FromBody] Invoice param)
        {
            Result ret = new Result();
            Invoice data = db.Invoice.Where(m => m.InvoiceID == param.InvoiceID).FirstOrDefault();

            if (data == null)
            {
                ret.Code = 0;
                ret.Message = "The record does not exist";
            }
            else
            {
                try
                {
                    data.InvoiceName = param.InvoiceName;
                    data.InvoiceDetailsID = param.InvoiceDetailsID;
                    data.Active = param.Active;
                    data.ModifiedOn = System.DateTime.Now;
                    db.SaveChanges();
                    ret.Code = 1;
                    ret.Message = "The record " + data.InvoiceID + " was updated sucessfully!";
                }
                catch (System.Exception ex)
                {
                    ret.Code = 0;
                    ret.Message = ex.Message.ToString();
                }
            }
            return ret;
        }

        // Invoice Details
        // GET: api/InvoiceDetail/{id}
        [Route("api/InvoiceDetail/{id}")]
        public Result Get(int id)
        {
            Result ret = new Result();
            InvoiceDetails data = db.InvoiceDetails.Where(m => m.InvoiceDetailsID == id).FirstOrDefault();

            if (data == null)
            {
                ret.Code = 0;
                ret.Message = "There is no data avaliable";
            }
            else
            {
                var jS = new JavaScriptSerializer();
                var model = new InvoiceDetailsModel();
                model.InvoiceDetailsID = data.InvoiceDetailsID;
                model.InvoiceConcept = data.InvoiceConcept;
                model.InvoiceTotalValue = data.InvoiceTotalValue;
                model.InvoiceDate = data.InvoiceDate.ToString(format: "dd/MM/yyyy", provider: System.Globalization.CultureInfo.InvariantCulture);
                ret.Code = 1;
                ret.Message = "Sucess!";
                ret.Data = jS.Serialize(model);
            }
            return ret;
        }

        // POST: api/InvoiceDetail/{id}
        [Route("api/Invoices/{id}")]
        [HttpPost]
        public Result CreateInvoiceDetail([FromBody] InvoiceDetails param, [FromUri] int id)
        {
            Result ret = new Result();
            InvoiceDetails data = db.InvoiceDetails.Where(m => m.InvoiceDetailsID == param.InvoiceDetailsID).FirstOrDefault();
            Invoice dataD = db.Invoice.Where(m => m.InvoiceID == id).FirstOrDefault();

            if (dataD == null)
            {
                ret.Code = 0;
                ret.Message = "The invoice does not exist";
            }
            else
            {
                if (data != null)
                {
                    ret.Code = 0;
                    ret.Message = "The record already exist";
                }
                else
                {
                    try
                    {
                        data = new InvoiceDetails
                        {
                            InvoiceConcept = param.InvoiceConcept,
                            InvoiceDate = param.InvoiceDate,
                            InvoiceTotalValue = param.InvoiceTotalValue,
                            CreatedOn = System.DateTime.Now,
                            ModifiedOn = System.DateTime.Now,
                            Active = true
                        };

                        db.InvoiceDetails.Add(data);
                        dataD.InvoiceDetailsID = data.InvoiceDetailsID;
                        db.SaveChanges();
                        ret.Code = 1;
                        ret.Message = "The record " + data.InvoiceDetailsID + " was created sucessfully!";
                    }
                    catch (System.Exception ex)
                    {
                        ret.Code = 0;
                        ret.Message = ex.Message.ToString();
                    }
                }
            }


            return ret;
        }

        // DELETE: api/InvoiceDetail/{id}
        [Route("api/InvoiceDetail/{id}")]
        [HttpDelete]
        public Result DeleteDetail(int id)
        {
            Result ret = new Result();
            InvoiceDetails data = db.InvoiceDetails.Where(m => m.InvoiceDetailsID == id).FirstOrDefault();

            if (data == null)
            {
                ret.Code = 0;
                ret.Message = "There record does not exist";
            }
            else
            {
                try
                {
                    db.InvoiceDetails.Remove(data);
                    db.SaveChanges();
                    ret.Code = 1;
                    ret.Message = "The record " + data.InvoiceDetailsID + " was deleted sucessfully!";
                } catch (System.Exception ex)
                {
                    ret.Code = 0;
                    ret.Message = ex.Message.ToString();
                }
            }
            return ret;
        }

        // Free resources
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

    }
}
