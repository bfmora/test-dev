﻿//-----------------------------------------------------------------------
// <copyright file="Result.cs" company="Development">
//     Copyright . All rights reserved.
// </copyright>
// <author>Brayan Mora</author>
// <description></description>
//-----------------------------------------------------------------------

namespace WebApiGMSTek.Models
{
    /// <summary>
    /// Entity Invoice Model (Service Response).
    /// </summary>
    public class InvoiceModel
    {
        #region Properties{ get; set; }

        public int InvoiceID { get; set; }
        public string InvoiceName { get; set; }
        public int? InvoiceDetailsID { get; set; }
        
        #endregion
    }
}
