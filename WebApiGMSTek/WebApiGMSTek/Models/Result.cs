﻿//-----------------------------------------------------------------------
// <copyright file="Result.cs" company="Development">
//     Copyright . All rights reserved.
// </copyright>
// <author>Brayan Mora</author>
// <description>Services Response</description>
//-----------------------------------------------------------------------

namespace WebApiGMSTek.Models
{
    /// <summary>
    /// Entity Result (Service Response).
    /// </summary>
    public class Result
    {
        #region Properties{ get; set; }

        /// <summary>
        /// 0: Error, 1: Success.
        /// </summary>
        public int Code { get; set; }


        /// <summary>
        /// Return Message.
        /// </summary>
        public string Message { get; set; }


        /// <summary>
        /// Return Data.
        /// </summary>
        public string Data { get; set; }

        #endregion
    }
}
