﻿//-----------------------------------------------------------------------
// <copyright file="Result.cs" company="Development">
//     Copyright . All rights reserved.
// </copyright>
// <author>Brayan Mora</author>
// <description></description>
//-----------------------------------------------------------------------

namespace WebApiGMSTek.Models
{
    /// <summary>
    /// Entity InvoiceDetails Model (Service Response).
    /// </summary>
    public class InvoiceDetailsModel
    {
        #region Properties{ get; set; }

        public int InvoiceDetailsID { get; set; }
        public string InvoiceConcept { get; set; }
        public int InvoiceTotalValue { get; set; }
        public string InvoiceDate { get; set; }
        
        #endregion
    }
}
